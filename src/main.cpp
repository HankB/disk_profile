#include <iostream>
#include <fstream>
#include <cstdlib>
#include <ctime>

#include "options.h"

using namespace std;

int main(int argc, char **argv)
{
    int dataBlockSize = 1024;    // size of each write (in ints)
    int writeCount = 1024; // number of time intervals to colllect
    int repeatCount = 512;     // number of buffer writes per timing interval
    string filename = "test.dat";

    cmd_line_options opts();

    clock_t previous; // used to measure clock() intervals
    clock_t now;      // for each write
    clock_t start;      // start time

    int *buffer = new int[dataBlockSize];         // data buffer to write
    clock_t *timeMarks = new clock_t[writeCount]; // buffer to store time marks

    cerr << "sizeof int = " << sizeof buffer[0] << endl;
    cerr << "sizeof buffer = " << sizeof buffer[0] * dataBlockSize << endl;
    cerr << "max random number = " << RAND_MAX << endl;
    cerr << "CLOCKS_PER_SEC = " << CLOCKS_PER_SEC << endl;
    cerr << "writeCount = " << writeCount << endl;
    cerr << "block size (bytes) = " << (sizeof buffer[0]) * dataBlockSize << endl;
    cerr << "blocks per time measurement = " << repeatCount << endl;
    cerr << "bytes per time measurement = " << 
            repeatCount * (sizeof buffer[0]) * dataBlockSize << endl;

    // populate buffer with random data
    srand(time(NULL));
    for (int i = 0; i < dataBlockSize; i++)
        buffer[i] = rand();

    // write the data
    ofstream testFile;
    testFile.rdbuf()->pubsetbuf(0, 0);
    testFile.open(filename);
    previous = start = clock();

    for (int i = 0; i < writeCount; i++)
    {
        for( int j=0; j<repeatCount; j++)
        {
            testFile.write((const char *)buffer, sizeof buffer[0] * dataBlockSize);
        }
        now = clock();
        timeMarks[i] = now - previous;
        previous = now;
    }

    testFile.close();
    remove(filename.c_str());

    for (int i = 0; i < writeCount; i++)
    {
        cout << timeMarks[i] << endl;
    }
    cerr << "total time = " << now - start << endl;

    delete[] buffer;
    delete[] timeMarks;
    return 0;
}