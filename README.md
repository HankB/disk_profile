# Disk Profile Benchmark

Profile read/write performance over time.

## Motivation

Results of the Gnome Disks benchmarks shows that disk performance varies over time as a benchmark proceeds. The purpose of this effort is to record the performance through a read/write benchmark so the performance variation can be observed.

## Requirements and environment.

Developed and tested in Linux (Debian, Raspbian.)

`cmake` and `g++`

## Status

Builds and runs. And is now abandoned.

While searching for ways to collect statistics during a run, it occurred to me that it makes more sense to

1. Use some tool like `sar` or `iostat` to collect statistics.
1. Collect statistics while any convenient benchmark is run.
1. Write some kind of wrapper that would invoke the benchmark, collect and parse the statistics and produce them in some sensible format.

I'm working on that at present and using my private Git server. I will probably put it up on a public server at some point. If interested, submit an issue and I can track that.

## Plans

* Add more output to identify test parameters and overall results.
* Add options to control parameters of execution such as
  * write block size (--buffsize, -b)
  * number of write blocks/sample (--sample_count, -s)
  * Total number of writes (--write_count, -w)

Reviewing this after three year hiatus.

## Build

```bash
mkdir build
cd build
cmake ..
make
./dpf
```

## Usage

`dpf` writes operating parameters to STDERR and test results to STDOUT.
